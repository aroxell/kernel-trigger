#!/bin/bash

# Clone the kernel-runs repo.
# Then, loop through all git repos/branches defined in `branches`
# and for each branch, find the sha of its HEAD using ls-remote
# and then update its branch in the kernel-runs repo, if needed.
#
# Also, write a .gitlab-ci.yml file into each branch into the kernel-runs
# repo, which simply includes the local branch-gitlab-ci.yml file here

# Do not mind the `cd dir; [...]; cd ..` recommendation:
# shellcheck disable=SC2103

set -ex

# Get the hash of any string
hash_string() {
  echo -n "$1" | sha1sum | cut -c1-40
}

if [ -n "${CI}" ]; then
  [ -d ~/.ssh ] || mkdir -p ~/.ssh
  ssh-keyscan gitlab.com > ~/.ssh/known_hosts
fi

if [ -z "${DRYRUN}" ]; then
  git clone git@gitlab.com:aroxell/kernel-runs.git
else
  git clone https://gitlab.com/aroxell/kernel-runs.git
fi

TMP_DIR=$(mktemp -d)

# Save all remote heads of all Git repos
awk '{ print $1 }' branches | sort -u | while read -r REPO_URL; do
  REPO_HASH=$(hash_string "${REPO_URL}")
  git ls-remote --heads "${REPO_URL}" > "${TMP_DIR}/${REPO_HASH}"
done

CWD=$(pwd)
while IFS= read -r line; do
  cd "${CWD}"
  echo "${line}"
  REPO_URL=$(echo "${line}" | awk '{print $1}')
  REPO_NAME=$(echo "${line}" | awk '{print $2}')
  BRANCH=$(echo "${line}" | awk '{print $3}')
  GIT_BRANCH=${REPO_NAME}-${BRANCH}
  cd kernel-runs
  if git show-branch "origin/${GIT_BRANCH}"; then
    # branch exists
    git checkout "${GIT_BRANCH}"
  else
    # create a new empty branch
    git checkout --orphan "${GIT_BRANCH}"
    git rm -rf . || true # this fails on an empty repo
  fi
  REPO_HASH=$(hash_string "${REPO_URL}")
  if grep -q "refs/heads/${BRANCH}$" "${TMP_DIR}/${REPO_HASH}"; then
    LS_REMOTE=$(grep "refs/heads/${BRANCH}$" "${TMP_DIR}/${REPO_HASH}")
  else
    echo "WARNING: ${BRANCH} does not exist at ${REPO_URL}; ignoring" >&2
    continue
  fi
  LATEST_SHA=$(echo "${LS_REMOTE}" | awk '{print $1}')
  echo "${LATEST_SHA}" > latest_sha
  cp ../branch-gitlab-ci-stub.yml .gitlab-ci.yml
  echo "${REPO_URL}" > git_repo
  echo "${BRANCH}" > git_branch
  echo "${REPO_NAME}" > repo_name
  if ! git status | grep -q "nothing to commit"; then
    git add .
    git commit -m "${REPO_NAME} ${BRANCH} ${LATEST_SHA}"
    if [ -z "${DRYRUN}" ]; then
      git push origin "${GIT_BRANCH}"
    fi
  fi
done < "branches"

rm -rf "${TMP_DIR}"
